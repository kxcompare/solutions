import java.util.stream.Stream;

public class App {

    private static boolean isOdd(char digit) {
        return digit == '1' || digit == '3' || digit == '5' || digit == '7' || digit == '9';
    }

    private static boolean checkNumber(int num) {
        return Integer.toString(num).chars()
                .mapToObj(i -> (char) i)
                .allMatch(App::isOdd);
    }

    public static void main(String[] args) {
        long count = IntStream.range(1000, 10_000)
                .filter(App::checkNumber)
                .count();
        System.out.println(count);
    }
}
