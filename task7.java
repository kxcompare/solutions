import java.util.stream.IntStream;

public class App {

    private static boolean isPalindrome(int number) {
        String numToStr = Integer.toString(number);
        for (int i = 0; i < numToStr.length() / 2; i++) {
            if (numToStr.charAt(i) != numToStr.charAt(numToStr.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        long count = IntStream.range(10_000, 100_000)
                .filter(App::isPalindrome)
                .count();
        System.out.println(count);
    }
}
